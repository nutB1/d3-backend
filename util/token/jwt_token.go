package token

import (
	"errors"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
)

// Different types of error returned by the VerifyToken function
var (
	ErrInvalidToken = errors.New("token is invalid")
	ErrExpiredToken = errors.New("token has expired")
)

// Payload contains the payload data of the token
type Payload struct {
	ID       uuid.UUID `json:"id"`
	Sub      string    `json:"sub"`
	Username string    `json:"username"`
	IssuedAt time.Time `json:"issued_at"`
}

// Valid checks if the token payload is valid or not
func (payload *Payload) Valid() error {
	return nil
}

// NewPayload creates a new token payload with a specific userid and username
func NewPayload(userId string, username string) (*Payload, error) {
	tokenID, err := uuid.NewRandom()
	if err != nil {
		return nil, err
	}

	payload := &Payload{
		ID:       tokenID,
		Sub:      userId,
		Username: username,
		IssuedAt: time.Now(),
	}
	return payload, nil
}

const minSecretKeySize = 32

// JWTMaker is a JSON Web Token maker
type JWTMaker struct {
	secretKey string
}

// NewJWTMaker creates a new JWTMaker
func NewJWTMaker(secretKey string) *JWTMaker {
	// if len(secretKey) < minSecretKeySize {
	// 	return nil, fmt.Errorf("invalid key size: must be at least %d characters", minSecretKeySize)
	// }
	return &JWTMaker{secretKey}
}

// CreateToken creates a new token for a specific userid and username
func (maker *JWTMaker) CreateToken(userId string, username string) (string, *Payload, error) {
	payload, err := NewPayload(userId, username)
	if err != nil {
		return "", payload, err
	}

	jwtToken := jwt.NewWithClaims(jwt.SigningMethodHS256, payload)
	token, err := jwtToken.SignedString([]byte(maker.secretKey))
	return token, payload, err
}

// VerifyToken checks if the token is valid or not
func (maker *JWTMaker) VerifyToken(token string) (*Payload, error) {
	keyFunc := func(token *jwt.Token) (interface{}, error) {
		_, ok := token.Method.(*jwt.SigningMethodHMAC)
		if !ok {
			return nil, ErrInvalidToken
		}
		return []byte(maker.secretKey), nil
	}

	jwtToken, err := jwt.ParseWithClaims(token, &Payload{}, keyFunc)
	if err != nil {
		verr, ok := err.(*jwt.ValidationError)
		if ok && errors.Is(verr.Inner, ErrExpiredToken) {
			return nil, ErrExpiredToken
		}
		return nil, ErrInvalidToken
	}

	payload, ok := jwtToken.Claims.(*Payload)
	if !ok {
		return nil, ErrInvalidToken
	}

	return payload, nil
}
