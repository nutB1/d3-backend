package main

import (
	"context"
	"fmt"
	"gobasic/config"
	"gobasic/db"
	sqlc "gobasic/db/sqlc"
	"log"

	_ "github.com/lib/pq"
)

func main() {
	// load configurations
	cfg, err := config.Load()
	if err != nil {
		log.Fatal(err.Error())
	}
	// open database connetion
	conn, err := db.OpenConnection(cfg)
	if err != nil {
		log.Fatal(err.Error())
	}
	// close database connection
	defer conn.Close()

	// new queries instance
	query := sqlc.New(conn)

	// Insert
	newParam := sqlc.CreateTransactionWithRowParams{
		Type:   sqlc.TransactionsTypeIncome,
		Title:  "Sarary",
		Amount: 25000,
	}

	row, err := query.CreateTransactionWithRow(context.Background(), newParam)

	if err != nil {
		panic(err)
	}

	fmt.Printf("New row: %+v\n", row)
}
