package main

import (
	"database/sql"
	"fmt"
	"gobasic/config"
	"gobasic/db"
	"log"

	"github.com/google/uuid"
	_ "github.com/lib/pq"
)

func main() {
	// load configurations
	cfg, err := config.Load()
	if err != nil {
		log.Fatal(err.Error())
	}
	// open database connetion
	conn, err := db.OpenConnection(cfg)
	if err != nil {
		log.Fatal(err.Error())
	}
	// close database connection
	defer conn.Close()

	Insert(conn, "income", "Sarary", 20000)
	Insert(conn, "expend", "Coffee", 50)

	Update(conn, "Coffee", 90)

	Delete(conn, "Coffee")

	Select(conn, "income")
}

func Insert(db *sql.DB, types string, title string, amount float64) {
	sql := `INSERT INTO "public"."transactions"
	(type, title, amount)
	VALUES ($1, $2, $3)`

	stmt, err := db.Prepare(sql)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer stmt.Close()

	result, err := stmt.Exec(types, title, amount)
	if err != nil {
		fmt.Println(err)
		return
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("Inserted", rowsAffected, "rows into the transactions table")

}

func Update(db *sql.DB, title string, amount float64) {
	sql := `UPDATE public.transactions SET amount = $1 WHERE title = $2`

	stmt, err := db.Prepare(sql)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer stmt.Close()

	result, err := stmt.Exec(amount, title)
	if err != nil {
		fmt.Println(err)
		return
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("Updated", rowsAffected, "rows into the transactions table")
}

func Delete(db *sql.DB, title string) {
	sql := `DELETE FROM public.transactions WHERE title = $1`

	stmt, err := db.Prepare(sql)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer stmt.Close()

	result, err := stmt.Exec(title)
	if err != nil {
		fmt.Println(err)
		return
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("Deleted", rowsAffected, "rows into the transactions table")
}

type Transaction struct {
	ID     uuid.UUID
	Type   string
	Title  string
	Amount float64
}

func Select(db *sql.DB, tp string) {
	sql := "SELECT id, type, title, amount FROM public.transactions WHERE type = $1"

	rows, err := db.Query(sql, tp)
	if err != nil {
		fmt.Println(err)
		return
	}

	defer rows.Close()

	for rows.Next() {
		var transaction Transaction
		err = rows.Scan(&transaction.ID, &transaction.Type, &transaction.Title, &transaction.Amount)
		if err != nil {
			fmt.Println(err)
			return
		}

		fmt.Println(transaction)
	}
}
