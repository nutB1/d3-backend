# Build Stage
FROM golang:1.20-alpine AS build
WORKDIR /app
COPY go.mod ./
COPY go.sum ./
RUN go mod download
COPY . .
ENV GOARCH=amd64
RUN go build -o /go/bin/api main.go

# Load migrate app Stage
FROM alpine:latest as migrate
WORKDIR /app
RUN apk --no-cache add curl
RUN curl -L https://github.com/golang-migrate/migrate/releases/download/v4.15.2/migrate.linux-amd64.tar.gz | tar xvz

# Final Stage
FROM alpine:latest as prod
WORKDIR /app
EXPOSE 8080

ENV TZ=Asia/Bangkok
ENV APP_MODE=production

RUN apk --no-cache add ca-certificates tzdata

COPY --from=migrate /app/migrate /app/migrate

COPY ./db/migrations /app/migrations
COPY --from=build /go/bin/api /app/api

CMD ["/app/api"]