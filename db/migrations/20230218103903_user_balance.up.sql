CREATE OR REPLACE VIEW public.user_balance AS
SELECT
    user_id,
    COALESCE(SUM(CASE WHEN type = 'income' THEN amount ELSE 0 END), 0)::float AS income,
    COALESCE(SUM(CASE WHEN type = 'expend' THEN amount ELSE 0 END), 0)::float AS expend,
    COALESCE(SUM(CASE WHEN type = 'income' THEN amount ELSE -amount END), 0)::float AS balance
FROM public.transactions
GROUP BY user_id;
