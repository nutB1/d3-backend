CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TYPE "transactions_type" AS ENUM (
  'expend',
  'income'
);


CREATE TABLE IF NOT EXISTS public.transactions (
  "id" uuid DEFAULT gen_random_uuid() PRIMARY KEY,
  "type" transactions_type NOT NULL,
  "title" varchar NOT NULL,
  "amount" numeric(10, 2) NOT NULL,
  "created_at" TIMESTAMPTZ NOT NULL DEFAULT current_timestamp
);

CREATE INDEX "transactions_type" ON public.transactions ("type");
CREATE INDEX "transactions_date" ON public.transactions ("created_at");
