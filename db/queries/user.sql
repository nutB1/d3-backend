-- name: GetUserByUsername :one
SELECT * FROM public.users WHERE username = $1;

-- name: CreateUser :one
INSERT INTO public.users ("username", "password")
VALUES ($1, $2) RETURNING *;