package db

import (
	"database/sql"
	"gobasic/config"
	"log"

	_ "github.com/lib/pq"
)

// dsn = postgres://user:passsword@host:port/database?sslmode=disable
func OpenConnection(cfg config.Config) (*sql.DB, error) {
	// Step 1: Open connection
	conn, err := sql.Open("postgres", cfg.DSN)
	if err != nil {
		return nil, err
	}

	// Step 2: test connection
	err = conn.Ping()
	if err != nil {
		return nil, err
	}

	log.Println("Successfully connected to the database!")
	return conn, nil
}
