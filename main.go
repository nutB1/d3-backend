package main

import (
	"gobasic/api"
	"gobasic/config"
	"gobasic/db"
	"log"
)

func main() {
	// load configurations
	cfg, err := config.Load()
	if err != nil {
		log.Fatal(err.Error())
	}
	// open database connetion
	conn, err := db.OpenConnection(cfg)
	if err != nil {
		log.Fatal(err.Error())
	}
	// close database connection
	defer conn.Close()

	server := api.NewServer(cfg, conn)

	log.Fatal(server.Start())
}
