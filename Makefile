ROOT_DIR := $(dir $(realpath $(lastword $(MAKEFILE_LIST))))

IMAGE_NAME=registry.gitlab.com/ost-pkru/2023/d3-backend/expend-api
IMAGE_VERSION=dev

export IMAGE_VERSION

# Run Go Application
run:
	go run main.go

# Start PostgreSQL Server on port 2345
pgu:
	docker run \
	--name expend-db-dev \
	-p 2345:5432 \
	-e POSTGRES_PASSWORD=mysecretpassword \
	-e POSTGRES_DB=expend-db \
	-v pg-expend-db-dev:/var/lib/postgresql/data \
	-d postgres:15-alpine

# Stop PostgreSQL Server
pgd:
	docker stop expend-db-dev

# Remove PostgreSQL Server and volume data
pgrm:
	docker rm expend-db-dev &&	\
	docker volume rm pg-expend-db-dev

# Create migration file
# make mgc filename=YOUR_FILE_NAME
mgc:
	docker run --rm -v "$(ROOT_DIR)/db/migrations":/migrations migrate/migrate -verbose create -ext sql -dir /migrations $(filename)

# Run migration up to latest version
mgu:
	docker run --rm --network host -v "$(ROOT_DIR)/db/migrations":/migrations migrate/migrate -verbose -path=/migrations/ -database "postgresql://postgres:mysecretpassword@localhost:2345/expend-db?sslmode=disable" up

# Run migration down by 1 version
mgd:
	docker run --rm --network host -v "$(ROOT_DIR)/db/migrations":/migrations migrate/migrate -verbose -path=/migrations/ -database "postgresql://postgres:mysecretpassword@localhost:2345/expend-db?sslmode=disable" down 1

# Use sqlc to generate code
sqlc:
	docker run --rm -v "$(ROOT_DIR):/src" -w /src kjconroy/sqlc:1.16.0 generate

doc:
	swag init --parseDependency -g api/api.go -o docs

build:
	docker image build -t $(IMAGE_NAME):$(IMAGE_VERSION) .

produ:
	docker-compose up -d

prodd:
	docker-compose down

prodrm:
	docker-compose down -v

# Build ubuntu server for CI/CD demo
build-demo-svr:
	docker image build -t somprasongd/demo-svr-ssh:22.04 -f Dockerfile-Ubuntu .

# Run ubuntu server for CI/CD demo
demo-svr:
	docker run -v /var/run/docker.sock:/var/run/docker.sock --privileged -d -p 22:22 --name=demo-svr somprasongd/demo-svr-ssh:22.04