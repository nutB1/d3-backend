package dtos

import (
	"errors"
	"strings"
)

type SignupRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func (r SignupRequest) Validate() error {
	errs := []string{}
	if len(r.Username) == 0 {
		errs = append(errs, "username is required")
	}
	if len(r.Password) == 0 {
		errs = append(errs, "password is required")
	}
	if len(errs) == 0 {
		return nil
	}
	return errors.New(strings.Join(errs, ", "))
}
