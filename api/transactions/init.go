package transactions

import (
	"gobasic/api/transactions/handler"
	db "gobasic/db/sqlc"

	"github.com/gofiber/fiber/v2"
)

func Init(r fiber.Router, q *db.Queries) {
	// create handler
	h := handler.NewTransactionHandler(q)

	transactions := r.Group("/transactions")

	transactions.Post("", h.Create)
	transactions.Get("", h.List)
	transactions.Get("/:id", h.Get)
	transactions.Patch("/:id", h.Update)
	transactions.Delete("/:id", h.Delete)
}
