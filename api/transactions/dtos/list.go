package dtos

import (
	db "gobasic/db/sqlc"
	"math"
)

type ListTransactionsFilter struct {
	Type      string `query:"type"`
	StartDate string `query:"start_date"`
	EndDate   string `query:"end_date"`
	Page      int    `query:"page"`
	PageSize  int    `query:"limit"`
}

type ListTransactionsResponse struct {
	Transactions []*TransactionResponse `json:"transactions"`
	Page         int                    `json:"page"`
	Limit        int                    `json:"limit"`
	Count        int                    `json:"count"`
	TotalPage    int                    `json:"total_page"`
}

func NewListTransactionsResponse(rows []db.Transaction, total int, page int, pageSize int) *ListTransactionsResponse {
	rr := NewTransactionResponses(rows)
	return &ListTransactionsResponse{
		Transactions: rr,
		Page:         page,
		Limit:        pageSize,
		Count:        len(rr),
		TotalPage:    int(math.Ceil(float64(total) / float64(pageSize))),
	}
}
