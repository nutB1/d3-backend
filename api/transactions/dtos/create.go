package dtos

import (
	"errors"
	db "gobasic/db/sqlc"
	"strings"
	"time"
)

type CreateTransactionRequest struct {
	Type   string  `json:"type"`
	Title  string  `json:"title"`
	Amount float64 `json:"amount"`
}

func (r *CreateTransactionRequest) Validate() error {
	errs := []string{}
	if len(r.Type) == 0 {
		errs = append(errs, "type is required")
	}
	if r.Type != "income" && r.Type != "expend" {
		errs = append(errs, "type must one of income, expend")
	}
	if len(r.Title) == 0 {
		errs = append(errs, "title is required")
	}
	if r.Amount == 0 {
		errs = append(errs, "amount must greater than 0")
	}
	if len(errs) == 0 {
		return nil
	}
	return errors.New(strings.Join(errs, ", "))
}

type TransactionResponse struct {
	Id        string    `json:"id"`
	Type      string    `json:"type"`
	Title     string    `json:"title"`
	Amount    string    `json:"amount"`
	CreatedAt time.Time `json:"created_at"`
}

func NewTransactionResponse(row db.Transaction) *TransactionResponse {
	return &TransactionResponse{
		Id:        row.ID.String(),
		Type:      string(row.Type),
		Title:     row.Title,
		Amount:    row.Amount,
		CreatedAt: row.CreatedAt,
	}
}

func NewTransactionResponses(rows []db.Transaction) []*TransactionResponse {
	rr := []*TransactionResponse{}
	for _, row := range rows {
		rr = append(rr, NewTransactionResponse(row))
	}
	return rr
}
