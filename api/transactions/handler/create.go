package handler

import (
	"gobasic/api/transactions/dtos"
	db "gobasic/db/sqlc"
	"gobasic/middlewares"
	"gobasic/util/token"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
)

// @Summary Create transaction
// @Description Create transaction
// @Tags Transaction
// @Accept json
// @Produce json
// @Param Authorization header string true "Bearer"
// @Param trabsaction body dtos.CreateTransactionRequest true "Transaction Data"
// @Failure 400
// @Failure 401
// @Failure 422
// @Failure 500
// @Success 200 {object} dtos.TransactionResponse
// @Router /transactions [post]
func (h transactionHandler) Create(c *fiber.Ctx) error {
	form := dtos.CreateTransactionRequest{}
	if err := c.BodyParser(&form); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": err.Error(),
		})
	}
	if err := form.Validate(); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": err.Error(),
		})
	}

	// get user id
	payload := c.Locals(middlewares.AuthorizationPayloadKey).(*token.Payload)

	params := db.CreateTransactionWithRowParams{
		Type:   db.TransactionsType(form.Type),
		Title:  form.Title,
		Amount: form.Amount,
		UserID: uuid.MustParse(payload.Sub),
	}
	row, err := h.queries.CreateTransactionWithRow(c.Context(), params)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": err.Error(),
		})
	}

	// serailize
	resp := dtos.NewTransactionResponse(row)
	return c.Status(fiber.StatusCreated).JSON(&resp)
}
