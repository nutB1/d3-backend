package handler

import (
	"database/sql"
	"gobasic/api/transactions/dtos"
	db "gobasic/db/sqlc"
	"gobasic/middlewares"
	"gobasic/util/token"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
)

// @Summary List transactions
// @Description You can filter transactions by listing them.
// @Tags Transaction
// @Accept json
// @Produce json
// @Param Authorization header string true "Bearer"
// @Param page query int false "Go to a specific page number. Start with 1"
// @Param limit query int false "Page size for the data"
// @Param start_date query string false "Filter by created_at greater than equal start date"
// @Param end_date query string false "Filter by created_at less than equal end date"
// @Failure 401
// @Failure 500
// @Success 200 {object} dtos.ListTransactionsResponse
// @Router /transactions [get]
func (h transactionHandler) List(c *fiber.Ctx) error {
	// get query strings
	startDate := c.Query("start_date")
	endDate := c.Query("end_date")
	page := c.QueryInt("page", 1)
	pageSize := c.QueryInt("limit", 10)

	// get user id
	payload := c.Locals(middlewares.AuthorizationPayloadKey).(*token.Payload)

	params := db.ListTransactionsParams{
		UserID:   uuid.MustParse(payload.Sub),
		Page:     int32((page - 1) * pageSize),
		PageSize: int32(pageSize),
	}
	if len(startDate) == 10 {
		d, err := time.Parse("2006-01-02", startDate)
		if err != nil {
			return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
				"error": "start_date invalid format",
			})
		}
		params.StartDate = sql.NullTime{Time: d, Valid: true}
	}
	if len(endDate) == 10 {
		d, err := time.Parse("2006-01-02", endDate)
		if err != nil {
			return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
				"error": "end_date invalid format",
			})
		}
		params.EndDate = sql.NullTime{Time: d, Valid: true}
	}

	rows, err := h.queries.ListTransactions(c.Context(), params)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": err.Error(),
		})
	}
	cParams := db.CountTransactionsParams{
		UserID:    params.UserID,
		StartDate: params.StartDate,
		EndDate:   params.StartDate,
	}
	total, err := h.queries.CountTransactions(c.Context(), cParams)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": err.Error(),
		})
	}

	// serailize
	resp := dtos.NewListTransactionsResponse(rows, int(total), page, pageSize)
	return c.JSON(resp)
}
