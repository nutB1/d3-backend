package handler

import (
	db "gobasic/db/sqlc"

	"github.com/gofiber/fiber/v2"
)

type TransanctionHandler interface {
	Create(c *fiber.Ctx) error
	List(c *fiber.Ctx) error
	Get(c *fiber.Ctx) error
	Update(c *fiber.Ctx) error
	Delete(c *fiber.Ctx) error
}

type transactionHandler struct {
	queries *db.Queries
}

func NewTransactionHandler(q *db.Queries) TransanctionHandler {
	return &transactionHandler{queries: q}
}
