package api

import (
	"database/sql"
	"fmt"
	"gobasic/api/dashboards"
	"gobasic/api/swagger"
	"gobasic/api/transactions"
	"gobasic/api/users"
	"gobasic/config"
	db "gobasic/db/sqlc"
	"gobasic/middlewares"
	"gobasic/util/token"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/gofiber/fiber/v2/middleware/requestid"
)

type server struct {
	config     config.Config
	app        *fiber.App
	queries    *db.Queries
	tokenMaker *token.JWTMaker
}

func NewServer(c config.Config, conn *sql.DB) *server {
	svr := &server{
		config:     c,
		app:        fiber.New(),
		queries:    db.New(conn),
		tokenMaker: token.NewJWTMaker(c.TokenSecret),
	}
	svr.setupRouter()
	return svr
}

func (s *server) setupRouter() {
	// Global Middlewares
	s.app.Use(cors.New())
	s.app.Use(requestid.New())
	s.app.Use(recover.New())
	s.app.Use(logger.New())

	// Register routes here
	api := s.app.Group("/api") // /api
	v1 := api.Group("/v1")     // /api/v1

	swagger.Init(v1, s.config)              // /api/v1/swagger
	users.Init(v1, s.queries, s.tokenMaker) // /api/v1/users

	// สร้าง router สำหรับที่ต้อง authen ก่อนใช้งาน
	authRouter := v1.Group("", middlewares.AuthMiddleware(s.tokenMaker))

	transactions.Init(authRouter, s.queries) // /api/v1/transactions
	dashboards.Init(authRouter, s.queries)   // /api/v1/dashboards

	// ทดสอบสร้าง /private ต้อง ผ่านการ authen ก่อน
	authRouter.Get("/private", func(c *fiber.Ctx) error {
		// วิธีเรียกใช้งาน payload ที่ส่งมาจาก AuthMiddleware
		authPayload := c.Locals(middlewares.AuthorizationPayloadKey).(*token.Payload)
		// ดึง user id ออกมา
		userId := authPayload.Sub
		// ดึง username ออกมา
		username := authPayload.Username
		msg := fmt.Sprintf("Access private route with user:%v, %v", userId, username)
		return c.SendString(msg)
	})
}

func (s *server) Start() error {
	return s.app.Listen(fmt.Sprintf(":%d", s.config.Port))
}
