package dtos

import db "gobasic/db/sqlc"

type UserBalanceResponse struct {
	Balance     float64 `json:"balance"`
	TotalIncome float64 `json:"total_income"`
	TotalExpend float64 `json:"total_expend"`
}

func NewUserBalanceResponse(row db.UserBalance) *UserBalanceResponse {
	return &UserBalanceResponse{
		Balance:     row.Balance,
		TotalIncome: row.Income,
		TotalExpend: row.Expend,
	}
}
