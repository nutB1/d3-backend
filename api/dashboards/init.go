package dashboards

import (
	"gobasic/api/dashboards/handler"
	db "gobasic/db/sqlc"

	"github.com/gofiber/fiber/v2"
)

func Init(r fiber.Router, q *db.Queries) {
	h := handler.NewDashboardHandler(q)

	dashboards := r.Group("/dashboards")
	dashboards.Get("/balances", h.UserBalance)
}
